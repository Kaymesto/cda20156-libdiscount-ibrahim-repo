<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!--directive :  -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Mes Annonces</title>
</head>
<body style="background-color: #b6b5b4;">
	<h1 style="text-align: center;">Mes annonces</h1>
	<tr>
		<td><p style="display: inline; margin-left: 10%; font: center;">Titre</p></td>
		<td><p style="display: inline; margin-left: 10%; font: center;">Ville</p></td>
		<td><p style="display: inline; margin-left: 7%; font: center;">Prix
				Unitaire</p></td>
		<td><p style="display: inline; margin-left: 2%; font: center;">Quantit�</p></td>
	</tr>
	<br>
	<c:forEach items="${sessionScope.user.listeAnnonces }" var="Annonces">

		<tr>
			<a href="./ServletAffichageAnnonce?id=${Annonces.idAnnonce}">
			<td><p style =" display : inline; margin-left : 10%;">${Annonces.titre }</p></a></td>
			<td ><p style =" display : inline; margin-left : 10%">${Annonces.ville }</p></td>
			<td ><p style =" display : inline; margin-left : 10%;">${Annonces.prixUnitaire}</p></td>
			<td ><p style =" display : inline; margin-left : 5%;">${Annonces.quantite}</p></td>
			<td style="text-align: center;">
			<a href="./ServletModifAnnonce?
			id=${Annonces.idAnnonce }"><p style =" display : inline; margin-left : 10%;">
			Modifier l'annonce</p></a></td>
			<td style="text-align: center;">
			<a href="./ServletSuppressionAnnonce?
			id=${Annonces.idAnnonce }"><p style =" display : inline; margin-left : 10%;">
			Supprimer l'annonce</p></a></td>

		</tr>
		<br>
		<br>
	</c:forEach>

	<br>
	<br>
	<br>
	<br>
	<br>

	<a href="RetourAccueilConnexion">
		<button type="button"
			style="float: left; margin-left: 40%; width: 200px; height: 50px;">
			<p style="font-size: 20px;">Retour � l'accueil</p>
		</button>
	</a>
</body>
</html>