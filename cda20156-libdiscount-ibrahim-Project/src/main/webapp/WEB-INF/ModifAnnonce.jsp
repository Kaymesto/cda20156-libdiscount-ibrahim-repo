<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body  style="background-color: #b6b5b4;">

<h1 style ="text-align:center;">Poster votre annonce</h1>
<br>
<form action="ServletModificationBDDAnnonce" method="post">

<div class = "container">
<label for="utitre" style = "font-size:40px; margin-left : 150px;"><b> <U>Titre : </U></b></label>
<input type="text" value=${annonce.titre}  style="float : right; width: 200px; height : 30px; margin-top : 10px; margin-left : 100px; margin-right : 300px;" name="utitre" required><br><br><br>

<label for="uville" style = "font-size:40px; margin-left : 150px;"><b> <U>Ville : </U></b></label>
<input type="text" value=${annonce.ville} style="  float : right;width: 200px; height : 30px; margin-top : 10px; margin-left : 100px; margin-right : 300px;" name="uville" required><br><br><br>

<label for="univeauscolaire" style = "font-size:40px; margin-left : 150px;"><b> <U>Niveau Scolaire : </U></b></label>
<input type="text" value=${annonce.niveauScolaire} style=" float : right;width: 200px; height : 30px; margin-top : 10px; margin-left : 100px; margin-right : 300px;" name="univeauscolaire" required><br><br><br>

<label for="uisbn" style = "font-size:40px; margin-left : 150px;"><b><U>ISBN : </U></b></label>
<input type="text" value=${annonce.isbn} style="float : right;width: 200px; height : 30px; margin-top : 10px; margin-right : 300px;" name="uisbn" required><br><br><br>

<label for="umaisonedition" style = "font-size:40px; margin-left : 150px;"><b><U>Maison d'�dition : </U></b></label>
<input type="text" value=${annonce.maisonEdition} style="float : right;width: 200px; height : 30px; margin-top : 10px; margin-left : 100px; margin-right : 300px;" name="umaisonedition" required>
<br><br><br>

<label for="udateedition" style = "font-size:40px; margin-left : 150px;"><b><U>Date d'�dition : </U></b></label>
<input type="text" value=${annonce.dateEdition} style="float : right;width: 200px; height : 30px; margin-top : 10px; margin-left : 100px; margin-right : 300px;" name="udateedition" required>
<br><br><br>
<label for="uprix" style = "font-size:40px; margin-left : 150px;"><b><U>Prix : </U></b></label>
<input type="number" value=${annonce.prixUnitaire} step="0.01" min<%= 0.00 %> style="float : right;width: 200px; height : 30px; margin-top : 10px; margin-left : 125px; margin-right : 300px;" name="uprix" required><br><br><br>

<label for="uquantite" style = "font-size:40px; margin-left : 150px;"><b><U>Quantit� : </U></b></label>
<input type="number" value=${annonce.quantite} step="1" min<%= 0 %>style="float : right;width: 200px; height : 30px; margin-top : 10px; margin-left : 125px; margin-right : 300px;" name="uquantite" required><br><br><br>

<label for="uremise" style = "font-size:40px; margin-left : 150px;"><b><U>Remise : </U></b></label>
<input type="number" value=${annonce.pourcentageRemise} step="0.01" min<%= 0.00 %> style="float : right;width: 200px; height : 30px; margin-top : 10px; margin-left : 125px; margin-right : 300px;" name="uremise" required><br><br><br>
<input id="idAnnonce" name="idAnnonce" type="hidden" value=${annonce.idAnnonce}>

<!-- <label for="imagefile" style = "float : center;font-size:40px; margin-left : 150px;"><b><u>Ajouter photos : </u></b>
<input type="file" name="imagefile" accept="image/jpeg, image/png" style="float : right;width: 200px; height : 30px; margin-top : 10px; margin-left : 125px; margin-right : 300px;"></label>

<br><br><br>  -->

<a href="RetourAccueilConnexion">
<button type="button" style="float : left; margin-left : 300px;  width : 200px; height : 50px;"><p style = "font-size:20px;">Retour � l'accueil</p></button>
</a>


<button type="submit" style="float : right; margin-right : 300px; width : 200px; height : 50px;"><p style = "font-size:20px;">Modifier</p></button>
<br><br><br><br>
</div>



</form>

</body>
</html>