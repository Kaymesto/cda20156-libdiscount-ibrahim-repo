package fr.afpa.Model;

import java.util.ArrayList;

import javax.persistence.NamedQuery;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import fr.afpa.Beans.Annonces;
import fr.afpa.Beans.Utilisateur;
import fr.afpa.cda.session.HibernateUtils;

public class GestionDAO {
	private Session s;
	private Transaction tx;
	
	public GestionDAO() {
	}
	
/**
 * Fonction permettant d'ajouter un utilisateur dans la BDD
 * @param u
 * @return
 */
	public boolean ajoutUtilisateur(Utilisateur u) {
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		boolean verif = true;
		ArrayList<Utilisateur> listeUtilisateur = new ArrayList();

		Query q = this.s.getNamedQuery("VerificationMail");
		q.setParameter("mail", u.getMail());
		listeUtilisateur = (ArrayList<Utilisateur>) q.getResultList();

		if (listeUtilisateur.size() > 0) {
			verif = false;
		} else {
			// Enregistrements de l'objet Utilisateur
			this.s.save(u);
			this.tx.commit();
		}
		this.s.close();
		return verif;
	}
	
	
	/**
	 * Fonction permettant de v�rifier l'existence de l'utilisateur et r�cup�rer ses informations
	 * @param id
	 * @param mdp
	 * @return
	 */
	public Utilisateur connexion(String id, String mdp) {
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		ArrayList<Utilisateur> listeUtilisateur = new ArrayList();
		Utilisateur u;

		Query q = this.s.getNamedQuery("VerificationUtilisateur");
		q.setParameter("mail", id);
		q.setParameter("motdepasse", mdp);
		
		listeUtilisateur = (ArrayList<Utilisateur>) q.getResultList();
		
		if(listeUtilisateur.size() >0 && listeUtilisateur.get(0).isActif() == true) {u = listeUtilisateur.get(0);}
		else { u = null; System.err.print("Utilisateur non trouv�");}
		this.s.close();

		return u;
	}

	
	/**
	 * Fonction permettant d'ajouter une annonce en BDD
	 * @param annonce
	 */
	public void ajoutAnnonce(Annonces annonce) {
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		this.s.save(annonce);
		this.tx.commit();
		this.s.close();
	}
	
	
	/**
	 * Fonction permettant de r�cup�rer toutes les annonces associ�es � un compte utilisateur
	 * @param u
	 */
	public void recuperationAnnonceUtilisateur(Utilisateur u) {
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		ArrayList<Annonces> listeAnnonces = new ArrayList();

		// D�but de la transaction

		// V�rification existence email
		Query q = this.s.getNamedQuery("RecupererAnnonceUtilisateur");
		q.setParameter("userid", u.getIdUtilisateur());
		
		listeAnnonces = (ArrayList<Annonces>) q.getResultList();
		u.setListeAnnonces(listeAnnonces);
		int i =0;
		System.out.println(i);
		for (Annonces annonces : listeAnnonces) {
			System.out.println(i);
			System.out.println(annonces.getTitre()+".");
		}
		this.s.close();
	}
	
	
	
	/**
	 * Fonction permettant de r�cup�rer toutes les informations associ�es � une annonce
	 * @param id
	 * @return
	 */
	public Annonces recuperationAnnonce(int id) {
		Annonces a = null;
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		ArrayList<Annonces> listeAnnonces = new ArrayList();


		Query q = this.s.getNamedQuery("RecupererUneAnnonce");
		q.setParameter("idAnnonce",id);
		
		listeAnnonces = (ArrayList<Annonces>) q.getResultList();
		if(listeAnnonces.size()>0) {
		a = listeAnnonces.get(0);}
		else {a = new Annonces();}
		this.s.close();
		System.out.println("TAILLE LISTE "+listeAnnonces.size());
		return a;
	}
	
	
	/**
	 * Fonction permettant de modifier les informations d'une annonce
	 * @param a
	 * @return
	 */
	public Annonces modifierAnnonce(Annonces a) {
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();


		Query q = this.s.getNamedQuery("ModifierAnnonce");
		q.setParameter("titre",a.getTitre());
		q.setParameter("ville", a.getVille());
		q.setParameter("niveauScolaire",a.getNiveauScolaire());
		q.setParameter("ISBN",a.getIsbn());
		q.setParameter("dateEdition",a.getDateEdition());
		q.setParameter("maisonEdition",a.getMaisonEdition());
		q.setParameter("prixUnitaire",a.getPrixUnitaire());
		q.setParameter("quantite",a.getQuantite());
		q.setParameter("pourcentageRemise",a.getPourcentageRemise());
		q.setParameter("idannonce",a.getIdAnnonce());
		System.out.println(a.getIdAnnonce());
		q.executeUpdate();
		this.s.close();
		return a;
	}
	
	
	/**
	 * Fonction permettant de d�sactiver une annonce
	 * @param idAnnonce
	 */
	public void archiverAnnonce(int idAnnonce) {
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();


		Query q = this.s.getNamedQuery("DesactiverAnnonce");
		q.setParameter("idannonce",idAnnonce);
		q.executeUpdate();
		this.s.close();

	}
	
	/**
	 * Fonction permettant de d�sactiver un utilisateur (uniquement utilisable par l'admin)
	 * @param idUser
	 */
	public void archiverUser(int idUser) {
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();


		Query q = this.s.getNamedQuery("DesactiverUtilisateur");
		q.setParameter("actif",false);
		q.setParameter("userid",idUser);
		q.executeUpdate();
		this.s.close();

	}
	
	
	/**
	 * Fonction permettant de r�cup�rer toutes les annonces qui ne sont pas d�sactiv�es
	 * @return
	 */
	public ArrayList<Annonces> recuperationAllAnnonces(){
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		ArrayList<Annonces> listeAnnonces = new ArrayList();

		
		Query q = this.s.getNamedQuery("RecupererAllAnnonces");
		
		listeAnnonces = (ArrayList<Annonces>) q.getResultList();
		this.s.close();
		return listeAnnonces;
	}
	
	/**
	 * Fonction permettant de r�cup�rer toutes les informations d'un utilisateur
	 * @param idUser
	 * @return
	 */
	public Utilisateur recuperationInfoUtilisateur(int idUser){
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		ArrayList<Utilisateur> listeAnnonces = new ArrayList();

		
		Query q = this.s.getNamedQuery("RecupererInfoUtilisateur");
		q.setParameter("idUtilisateur", idUser);
		listeAnnonces = (ArrayList<Utilisateur>) q.getResultList();
		this.s.close();
		return listeAnnonces.get(0);
	}
	
	
	/**
	 * Fonction permettant de r�cup�rer toutes les annonces apr�s filtrage de l'utilisateur
	 * @param titre
	 * @param ville
	 * @param niveauScolaire
	 * @return
	 */
	public ArrayList<Annonces> recuperationAnnoncesFiltre(String titre, String ville, String niveauScolaire){
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		ArrayList<Annonces> listeAnnonces = new ArrayList();

		
		Query q = this.s.getNamedQuery("FiltrerAllAnnonces");
		q.setParameter("titre", '%'+titre+'%');
		q.setParameter("ville", '%'+ville+'%');
		q.setParameter("niveauScolaire", '%'+niveauScolaire+'%');
		
		listeAnnonces = (ArrayList<Annonces>) q.getResultList();
		this.s.close();
		return listeAnnonces;
	}
	
	/**
	 * Fonction permettant d'afficher tous les utilisateurs (uniquement pour l'administrateur)
	 * @return
	 */
	public ArrayList<Utilisateur> recuperationAllUsers(){
		this.s = HibernateUtils.getSession();
		this.tx = this.s.beginTransaction();
		ArrayList<Utilisateur> listeUsers = new ArrayList();

		
		Query q = this.s.getNamedQuery("RecupererAllUtilisateur");

		
		listeUsers = (ArrayList<Utilisateur>) q.getResultList();
		this.s.close();
		return listeUsers;
	}
}
