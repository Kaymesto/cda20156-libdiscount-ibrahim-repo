package fr.afpa.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Beans.Annonces;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class ServletModifAnnonce
 */
public class ServletModifAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idAnnonceString = request.getParameter("id");
		Annonces a = null;
		int idAnnonce = 0;
		try {
			idAnnonce = Integer.parseInt(idAnnonceString);
		}
		catch(Exception e) {}
		GestionDAO gdao = new GestionDAO();
		a = gdao.recuperationAnnonce(idAnnonce);
		
		request.setAttribute("annonce", a);
		request.getRequestDispatcher("/WEB-INF/ModifAnnonce.jsp").forward(request, response);
	
	
	}
	
	



}
