package fr.afpa.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Beans.Annonces;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class AllAnnonces
 */
public class AllAnnonces extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		GestionDAO gdao = new GestionDAO();
		ArrayList<Annonces > listeAnnonces = gdao.recuperationAllAnnonces();
		if (listeAnnonces == null) {
			listeAnnonces = new ArrayList();
		}
		request.setAttribute("listeAllAnnonces", listeAnnonces);
		request.getRequestDispatcher("/WEB-INF/ListerAllAnnonces.jsp").forward(request, response);

	}

}
