package fr.afpa.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class CreateUser
 */
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		boolean verificationCreation = false;
		String nom = request.getParameter("uname");
		String prenom = request.getParameter("usurname");
		String nomLibrairie = request.getParameter("ulibrary");
		String adresse = request.getParameter("uadresse");
		String mail = request.getParameter("umail");
		String pwd = request.getParameter("upassword");
		String tel = request.getParameter("utelephone");
		
		GestionDAO gdao = new GestionDAO();
		Utilisateur u = new Utilisateur(nom,prenom,nomLibrairie,adresse,mail,pwd,tel);
		u.setActif(true);
		verificationCreation = gdao.ajoutUtilisateur(u);
		
		if(verificationCreation == true) {
			request.getRequestDispatcher("ConfirmationCreationCompte.html").forward(request, response);
		}
		else {
			String erreur = "Cette adresse-email est d�ja utilis�e.";
			request.setAttribute("erreur", erreur);
			request.getRequestDispatcher("CreationCompte.jsp").forward(request, response);
		}
	}

}
