package fr.afpa.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Beans.Annonces;
import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class ServletModificationBDDAnnonce
 */
public class ServletModificationBDDAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idAnnonceString = request.getParameter("idAnnonce");
		int id = 0;
		String titre = request.getParameter("utitre");
		String ville = request.getParameter("uville");
		String niveauScolaire = request.getParameter("univeauscolaire");
		String iSBN = request.getParameter("uisbn");
		String dateEdition = request.getParameter("udateedition");
		String maisonEdition = request.getParameter("umaisonedition");
		String prix = request.getParameter("uprix");
		prix.replace(",", ".");
		String quantite = request.getParameter("uquantite");
		String remise = request.getParameter("uremise");
		remise.replace(",", ".");
		float price = 0;
		int quantity = 0;
		float discount = 0;
		
		try {
			id = Integer.parseInt(idAnnonceString);
			price = Float.valueOf(prix);
			quantity = Integer.parseInt(quantite);
			discount = Float.valueOf(remise);
		}
		catch(Exception e) {}
		
		Utilisateur u = (Utilisateur) request.getSession().getAttribute("user");
		Annonces a = new Annonces(titre,niveauScolaire, ville, iSBN, dateEdition, maisonEdition, price, quantity, discount, "");
		a.setIdAnnonce(id);
		GestionDAO gdao = new GestionDAO();
		gdao.modifierAnnonce(a);
		System.out.println(titre+";"+ville+";"+niveauScolaire);
		
		request.getRequestDispatcher("ConfirmationModificationAnnonce.html").forward(request, response);
		
		
		
		}

}
