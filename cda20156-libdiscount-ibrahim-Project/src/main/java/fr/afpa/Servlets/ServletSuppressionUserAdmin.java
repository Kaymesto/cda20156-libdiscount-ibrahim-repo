package fr.afpa.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class ServletSuppressionUserAdmin
 */
public class ServletSuppressionUserAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idUserString = request.getParameter("id");
		int idUser = 0;
		try {idUser = Integer.parseInt(idUserString);}
		catch(Exception e) {}
		GestionDAO gdao = new GestionDAO();
		gdao.archiverUser(idUser);
		
		request.getRequestDispatcher("AllUsersAdmin").forward(request, response);

	
	}



}
