package fr.afpa.Servlets;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import fr.afpa.Beans.Annonces;
import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;

import javax.servlet.http.Part;


@WebServlet("/upload")
@MultipartConfig
public class PosterAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String IMAGES_FOLDER = "/Images_ventes";   
    public String uploadPath;    
    
    /*
     * Si le dossier de sauvegarde de l'image n'existe pas, on demande sa cr�ation.
     */ 
    @Override
    public void init() throws ServletException {
        uploadPath = getServletContext().getRealPath( IMAGES_FOLDER );
        File uploadDir = new File( uploadPath );
        if ( ! uploadDir.exists() ) uploadDir.mkdir();
    }
    
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		String titre = request.getParameter("utitre");
		String ville = request.getParameter("uville");
		String niveauScolaire = request.getParameter("univeauscolaire");
		String iSBN = request.getParameter("uisbn");
		String dateEdition = request.getParameter("udateedition");
		String maisonEdition = request.getParameter("umaisonedition");
		String prix = request.getParameter("uprix");
		String quantite = request.getParameter("uquantite");
		String remise = request.getParameter("uremise");
		float price = 0;
		int quantity = 0;
		
		float discount = 0;
		try {
			price = Float.parseFloat(prix);
			quantity = Integer.parseInt(quantite);
			discount = Float.parseFloat(remise);
		}
		catch(Exception e) {}
		String fullPath = "";
//		for (Part part : request.getParts()) {
//			 part = request.getPart("imagefile");
//            String fileName = getFileName( part );
//            fullPath = uploadPath + File.separator + fileName;
//            part.write( fullPath );
//		}
		
        

		Utilisateur u = (Utilisateur) request.getSession().getAttribute("user");
		Annonces a = new Annonces(titre,niveauScolaire, ville, iSBN, dateEdition, maisonEdition, price, quantity, discount, fullPath);
		a.setUser(u);
		GestionDAO gdao = new GestionDAO();
		gdao.ajoutAnnonce(a);
		
		request.getRequestDispatcher("ConfirmationcreationAnnonce.html").forward(request, response);
		
		
	}
	
	/*
     * R�cup�ration du nom du fichier dans la requ�te.
     */
    private String getFileName( Part part ) {
        for ( String content : part.getHeader( "content-disposition" ).split( ";" ) ) {
            if ( content.trim().startsWith( "filename" ) )
                return content.substring( content.indexOf( "=" ) + 2, content.length() - 1 );
        }
        return "Default.file";
    }

}
