package fr.afpa.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class ListAnnonceUser
 */
public class ListAnnonceUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Utilisateur u = (Utilisateur) session.getAttribute("user");
		GestionDAO gdao = new GestionDAO();
		gdao.recuperationAnnonceUtilisateur(u);
		request.setAttribute("users", u);
		request.getRequestDispatcher("/WEB-INF/ListerAnnonceUtilisateur.jsp").forward(request, response);
		
	}



}
