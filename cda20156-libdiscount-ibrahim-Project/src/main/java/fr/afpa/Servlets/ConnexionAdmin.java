package fr.afpa.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class ConnexionAdmin
 */
public class ConnexionAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("ulogin");
		String mdp = request.getParameter("upwd");
		Utilisateur u = null;
		GestionDAO gdao = new GestionDAO();
		u = gdao.connexion(id, mdp);
		
		if(u != null && "ADMIN".equals(id) && "ADMIN".equals(mdp)) {
			HttpSession session = request.getSession();
			session.setAttribute("user", u);
			request.setAttribute("users", u);
			System.out.println(u);
			gdao.recuperationAnnonceUtilisateur(u);
			request.getRequestDispatcher("/WEB-INF/AccueilAdmin.jsp").forward(request, response);
		}
		else {
			request.setAttribute("erreurLogin", "Nom de compte ou mot de passe incorrect");
			request.getRequestDispatcher("/WEB-INF/ConnexionAdmin.jsp").forward(request, response);
		}
		}

}
