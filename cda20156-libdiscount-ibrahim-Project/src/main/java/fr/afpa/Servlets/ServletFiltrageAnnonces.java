package fr.afpa.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Beans.Annonces;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class ServletFiltrageAnnonces
 */
public class ServletFiltrageAnnonces extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String titre = request.getParameter("ufiltretitre").toLowerCase();
		String ville = request.getParameter("ufiltreville").toLowerCase();
		String niveauScolaire = request.getParameter("ufiltreniveau").toLowerCase();
		GestionDAO gdao = new GestionDAO();
		ArrayList<Annonces > listeAnnonces = gdao.recuperationAnnoncesFiltre(titre,ville,niveauScolaire);
		if (listeAnnonces == null) {
			listeAnnonces = new ArrayList();
		}
		request.setAttribute("listeAllAnnonces", listeAnnonces);
		
		request.getRequestDispatcher("/WEB-INF/ListerAllAnnonces.jsp").forward(request, response);
	}

}
