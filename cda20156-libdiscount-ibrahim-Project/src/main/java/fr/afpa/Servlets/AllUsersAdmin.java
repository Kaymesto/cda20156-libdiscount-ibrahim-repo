package fr.afpa.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Beans.Annonces;
import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class AllAnnonces
 */
public class AllUsersAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		GestionDAO gdao = new GestionDAO();
		ArrayList<Utilisateur > listeUtilisateur = gdao.recuperationAllUsers();
		if (listeUtilisateur == null) {
			listeUtilisateur = new ArrayList();
		}
		request.setAttribute("listeUsers", listeUtilisateur);
		request.getRequestDispatcher("/WEB-INF/ListerAllUsersAdmin.jsp").forward(request, response);

	}

}
