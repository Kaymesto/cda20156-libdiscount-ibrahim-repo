package fr.afpa.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class AffichageInfoVendeur
 */
public class AffichageInfoVendeur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("idAnnonce");
		int idAnnonce = 0;
		try { idAnnonce = Integer.parseInt(id);}
		catch(Exception e) {}
		Utilisateur u = null;
		GestionDAO gdao = new GestionDAO();
		u = gdao.recuperationInfoUtilisateur(idAnnonce);
		System.out.println("id Annonce : "+idAnnonce);
		request.setAttribute("user", u);
		request.getRequestDispatcher("/WEB-INF/AfficherProprietaireAnnonce.jsp").forward(request, response);
	}



}
