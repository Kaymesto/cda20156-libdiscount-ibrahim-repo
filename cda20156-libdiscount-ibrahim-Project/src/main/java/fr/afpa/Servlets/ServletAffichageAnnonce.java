package fr.afpa.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.Beans.Annonces;
import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;

/**
 * Servlet implementation class ServletAffichageAnnonce
 */
public class ServletAffichageAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAffichageAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idString = request.getParameter("id");
		int id =0;
		try {
			id = Integer.parseInt(idString);
		}
		catch(Exception e){}
		GestionDAO gdao = new GestionDAO();
		Annonces a = null;
		a = gdao.recuperationAnnonce(id);
		if(a == null) {a=new Annonces();}
		request.setAttribute("annonce", a);
		request.setAttribute("idCreateur", a.getUser().getIdUtilisateur());
		request.getRequestDispatcher("/WEB-INF/AfficherAnnonce.jsp").forward(request, response);
	}



}
