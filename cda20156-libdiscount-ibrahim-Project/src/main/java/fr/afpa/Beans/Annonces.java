package fr.afpa.Beans;

import java.awt.Image;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Annonces")
@NamedQuery(name="RecupererAllAnnonces", query="select a from Annonces a where a.archive = false") 
@NamedQuery(name="RecupererUneAnnonce", query="select a from Annonces a where a.idAnnonce = :idAnnonce") 
@NamedQuery(name="RecupererAnnonceUtilisateur", query="select a from Annonces a where a.user.idUtilisateur = :userid and a.archive = false") // Pour l'admin
@NamedQuery(name="ModifierAnnonce", query="update Annonces a set titre = :titre,ville = :ville, niveauScolaire = :niveauScolaire, "
		+ "isbn = :ISBN, dateEdition = :dateEdition, maisonEdition = :maisonEdition, prixUnitaire = :prixUnitaire,"
		+ "quantite = :quantite, pourcentageRemise = :pourcentageRemise where a.idAnnonce = :idannonce") 
@NamedQuery(name="DesactiverAnnonce", query="update Annonces a set a.archive = true where idAnnonce = :idannonce") 

@NamedQuery(name="FiltrerAllAnnonces", query="select a from Annonces a WHERE LOWER(a.titre) LIKE :titre"
		+ " and LOWER(a.ville) LIKE :ville"
		+ " and LOWER(a.niveauScolaire) LIKE :niveauScolaire") // Pour l'admin

public class Annonces {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_idAnnonce")
	private int idAnnonce;
	@Column(name= "titre")
	private String titre;
	@Column(name= "niveauScolaire")
	private String niveauScolaire;
	@Column(name= "ville")
	private String ville;
	@Column(name= "isbn")
	private String isbn;
	@Column(name= "dateEdition")
	private String dateEdition;
	@Column(name= "maisonEdition")
	private String maisonEdition;
	@Column(name= "prixUnitaire")
	private float prixUnitaire;
	@Column(name= "quantite")
	private int quantite;
	@Column(name= "pourcentageRemise")
	private float pourcentageRemise;
	@Column(name= "cheminImage")
	private String cheminImage;
	@Column(name= "archive")
	private boolean archive;
	@ManyToOne(
			cascade= {CascadeType.ALL}
			)
	@JoinColumn(name="pk_utilisateur_id")
	private Utilisateur user;
	
	public Annonces(String titre, String niveauScolaire, String ville, String isbn, String dateEdition, String maisonEdition, float prixUnitaire,
			int quantite, float pourcentageRemise, String cheminImage) {
	
		this.titre = titre;
		this.niveauScolaire = niveauScolaire;
		this.ville = ville;
		this.isbn = isbn;
		this.dateEdition = dateEdition;
		this.maisonEdition = maisonEdition;
		this.prixUnitaire = prixUnitaire;
		this.quantite = quantite;
		this.pourcentageRemise = pourcentageRemise;
		this.cheminImage = cheminImage;

	}

}
