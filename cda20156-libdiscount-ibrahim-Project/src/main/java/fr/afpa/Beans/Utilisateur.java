package fr.afpa.Beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

@Entity
@NamedQuery(name="RecupererAllUtilisateur", query="select u from Utilisateur u where u.actif = 'true'") // Pour l'admin
@NamedQuery(name="RecupererInfoUtilisateur", query="select u from Utilisateur u where u.idUtilisateur = :idUtilisateur and u.actif = 'true' ") // Pour l'admin
@NamedQuery(name="VerificationMail", query="select u from Utilisateur u where u.mail = :mail") // Pour la cr�ation de compte
@NamedQuery(name="VerificationUtilisateur", query="select u from Utilisateur u where u.mail = :mail and u.motDePasse = :motdepasse and u.actif = 'true'") // Pour la connexion
@NamedQuery(name="DesactiverUtilisateur", query="update Utilisateur u set u.actif = :actif where u.idUtilisateur = :userid") // Pour l'admin

@Table(name="Utilisateur", uniqueConstraints=
@UniqueConstraint(columnNames= {"pk_utilisateur_id","mail"}))
public class Utilisateur {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_utilisateur_id")
	private int idUtilisateur;
	@Column(name="nom")
	private String nom;
	@Column(name="prenom")
	private String prenom;
	@Column(name="nomLibrairie")
	private String nomLibrairie;
	@Column(name="adresse")
	private String adresse;
	@Column(name="mail")
	private String mail;
	@Column(name="motDePasse")
	private String motDePasse;
	@Column(name="telephone")
	private String telephone;
	@Column(name="droit")
	private int droit;
	@Column(name="actif")
	private boolean actif;
	@OneToMany(mappedBy="user")
	private List <Annonces> listeAnnonces;
	
	public Utilisateur(String nom, String prenom, String nomLibrairie, String adresse, String mail, String motDePasse, String telephone) {
		this.nom = nom;
		this.prenom = prenom;
		this.nomLibrairie = nomLibrairie;
		this.adresse = adresse;
		this.mail = mail;
		this.motDePasse = motDePasse;
		this.telephone = telephone;
	}
	
	public Utilisateur() {
		this.droit = 1;
		this.actif = true;
	}
	
	@Override
	public String toString() {
		return "Nom : "+nom
				+"\nPrenom : "+prenom
				+"\nNom de Librairie : "+nomLibrairie
				+"\nAdresse : "+adresse
				+"\nAdresse Mail : "+mail
				+"\nTelephone : "+telephone;
	}

}
