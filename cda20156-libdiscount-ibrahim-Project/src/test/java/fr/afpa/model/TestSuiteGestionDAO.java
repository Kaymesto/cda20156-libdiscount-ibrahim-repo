package fr.afpa.model;

import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestSuiteGestionDAO  extends TestCase {
	
	public TestSuiteGestionDAO() {
		super();
	}

	public static TestSuite suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTest(new TestSuite(TestGestionDAO.class));
		
		return suite;
	}
	
	public static void main(String [] args) {
		junit.textui.TestRunner.run(suite());
	}
}
