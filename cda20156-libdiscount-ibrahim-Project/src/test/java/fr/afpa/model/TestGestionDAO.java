package fr.afpa.model;

import java.util.ArrayList;

import fr.afpa.Beans.Annonces;
import fr.afpa.Beans.Utilisateur;
import fr.afpa.Model.GestionDAO;
import junit.framework.TestCase;

public class TestGestionDAO extends TestCase {
	private GestionDAO gdao;

	protected void setUp() throws Exception {
		super.setUp();
		gdao = new GestionDAO();
	}

	public void testRecuperationAnnonceUtilisateur() {
		Utilisateur u = new Utilisateur();
		u.setIdUtilisateur(10);
		gdao.recuperationAnnonceUtilisateur(u);
		boolean verif = (u.getListeAnnonces().size() > 0);
		assertEquals(true, verif);

	}

	public void testrecuperationAllAnnonces() {
		ArrayList<Annonces> listeAnnonces = new ArrayList();
		listeAnnonces = gdao.recuperationAllAnnonces();
		boolean verif = (listeAnnonces.size() > 0);
		assertEquals(true, verif);

	}

	public void testrecuperationInfoUtilisateur() {
		Utilisateur u = null;
		u = gdao.recuperationInfoUtilisateur(10);
		boolean verif = (u != null);
		assertEquals(true, verif);

	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
