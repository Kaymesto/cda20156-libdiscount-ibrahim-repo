Bienvenue dans l'application LibDiscount réalisée par Ibrahim EL Kadiri au sein de l'afpa (cda-20156).

Cette application est une application de mise en relation entre libraires afin de pouvoir s'approvisionner et également
écouler leur stocks de livres entre eux.

Les pré-requis à l'utilisation de cette application sont :
- L'installation de la JDK / JRE
- La création d'une base de donnée sur PGAdmin nommée : "LibDiscount" ainsi que d'un compte superUser nommé : "postgres"
 avec comme mot de passe : "ADMINS".
- Entrer le script fourni dans le script de votre BDD afin de créer le compte Admin.
- L'installation de TOMCAT à la derniere version.
- Ouvrir le fichier starup.bat dans le dossier bin de tomcat (Attention à ce que Eclipse soit bien éteint car il peut y avoir un conflit).
- Ajouter le fichier war dans le dossier webApps de Tomcat.
- Vérifier que le déploiement a bien été réalisé en mettant l'url suivant : http://localhost:8080/cda20156-libDiscount-EE/
ce qui vous permettra d'atterir à l'accueil du site.


Les utilisateurs lambda pourront :
- créer un compte
- se connecter
- poster des annonces
- voir leurs annonces
- modifier / supprimer leurs annonces
- voir les annonces des autres ainsi que leur contact

L'Administrateur pourra se connecter via un lien caché :
- il faut entrer lorsque vous êtes à l'accueil du site dans l'url : http://localhost:8080/cda20156-libDiscount-EE/HiddenAccess 
ce qui vous permettra de vous connecter.
Une fois connecté, vous pourrez : désactiver les utilisateurs, leurs annonces et également voir les différentes annonces postées.



Pour tout complément d'informations ou bugs à corriger:
 Merci de contacter l'administrateur par email : ibrahim.elkadiri@yahoo.fr


Futures mises à jours :
- Améliorations graphiques.
- Améliorations de la sécurité.